package com.example.tomek93.tictactoe;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import static java.security.AccessController.getContext;



public class MainActivity extends AppCompatActivity {

    String FILENAME = "ox_save";
    public static final String PREFS_NAME = "ox_save";
    ImageView board = null;
    TextView textView = null;
    static int[][] boardValues = null;
    static int turn = 0;
    static boolean firstCycle = true;
    static int computerPlayer = 0;
    static boolean gameEnabled = true;
    Random rnd = new Random();
    static int emptyCells = 9;

    static
    {
        if(boardValues == null)
        {
            boardValues = new int[3][3];
        }
    }

    MainActivity()
    {
        if(firstCycle)
        {
            try {
                readState();
                if (turn == -1)
                    newGame();
            }
            catch(Exception ex)
            {
                newGame();
            }
            firstCycle= false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textView = (TextView) findViewById(R.id.textView);
        this.board = (ImageView) findViewById(R.id.imageView);
        this.board.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    double relX = event.getX() / board.getWidth();
                    double relY = event.getY() / board.getHeight();
                    boardClick(relX, relY);
                }
                return true;
            }
        });
        //Log.e("change", "change");
        setState();
        checkState();
    }

    void boardClick(double relX, double relY)
    {
        if( turn == computerPlayer || !gameEnabled )
            return;
        int cell = cellIndexFromCoords(relX, relY);

        if(cell >= 0)
        {
            if( boardValues[cell%3][cell/3] == 0 )
            {
               cellClick(cell);
               if(computerPlayer == turn)
               {
                  computerTurn();
               }
            }
        }
    }

    void cellClick(int cell)
    {
        boardValues[cell%3][cell/3] = turn;
        turn = (turn %2) + 1;
        emptyCells--;
        checkState();
        setState();
        //saveState();
    }

    void checkState()
    {
        if(isWin(1))
        {
            gameEnabled = false;
            textView.setText(getResources().getString(R.string.winner)+": O");
        }
        else if(isWin(2))
        {
            gameEnabled = false;
            textView.setText(getResources().getString(R.string.winner)+": X");
        }
    }

    boolean isWin(int player)
    {
        for(int i= 0; i< 3; i++)
        {
            if( (boardValues[i][0] == player) && (boardValues[i][1] == player) && (boardValues[i][2] == player) )
                return true;
            if( (boardValues[0][i] == player) && (boardValues[1][i] == player) && (boardValues[2][i] == player) )
                return true;
        }
        if( (boardValues[0][0] == player) && (boardValues[1][1] == player) && (boardValues[2][2] == player) )
            return true;
        if( (boardValues[2][0] == player) && (boardValues[1][1] == player) && (boardValues[0][2] == player) )
            return true;
        return false;
    }

    int cellIndexFromCoords(double relX, double relY)
    {
        if( relY < 0.4)
            return -1;
        int x = 0;
        int y = 0;

        if( relY < 0.58)
            y = 0;
        else if( relY > 0.79)
            y = 2;
        else
            y = 1;

        if( relX < 0.39)
            x = 0;
        else if( relX > 0.67)
            x = 2;
        else
            x = 1;

        return y*3 + x;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newGameMenu:
                //Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
                newGame();
                setState();
                return true;
            case R.id.autoplayerMenu:
                //Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
                if(computerPlayer == 0) {
                    computerPlayer = turn;
                    item.setChecked(true);
                    computerTurn();
                }
                else{
                    computerPlayer = 0;
                    item.setChecked(false);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void newGame()
    {
        for(int i= 0; i< 3; i++)
            for( int j= 0; j< 3; j++)
                boardValues[i][j] = 0;
        turn = rnd.nextInt(2)+1;
        gameEnabled = true;
        emptyCells = 9;
     //   saveState();

        if(computerPlayer == turn)
        {
            computerTurn();
        }
    }

    void computerTurn()
    {
        if( !gameEnabled || (emptyCells <= 0) )
            return;

        if(emptyCells == 9)
        {
            cellClick(0);
            return;
        }

        int bestCell = -1;
        int best = -10000;

        for( int i= 0; i< 9; i++)
        {
            if( boardValues[i%3][i/3] == 0 )
            {
                boardValues[i%3][i/3] = computerPlayer;
                int minimaxVal =  minimax(0, false);
                boardValues[i%3][i/3] = 0;
                if(minimaxVal > best)
                {
                    best = minimaxVal;
                    bestCell = i;
                }
            }
        }

        cellClick(bestCell);
    }

    int minimax(int depth, boolean isMax)
    {
        if(isWin(computerPlayer))
            return 10;
        if(isWin(computerPlayer%2 +1))
            return -10;

        if(emptyCells - depth -1 == 0)
            return 0;

        if(isMax)
        {
            int best = -1000;
            for( int i= 0; i< 9; i++)
            {
                if( boardValues[i%3][i/3] == 0 )
                {
                    boardValues[i%3][i/3] = computerPlayer;
                    best = Math.max(best, minimax(depth+1, !isMax));
                    boardValues[i%3][i/3] = 0;
                }
            }
            return best;
        }
        else
        {
            int best = 1000;
            for( int i= 0; i< 9; i++)
            {
                if( boardValues[i%3][i/3] == 0 )
                {
                    boardValues[i%3][i/3] = computerPlayer%2 +1;
                    best = Math.min(best, minimax(depth+1, !isMax));
                    boardValues[i%3][i/3] = 0;
                }
            }
            return best;
        }
    }

    void setState()
    {
        for(int i= 0; i< 9; i++)
        {
            int id = getResources().getIdentifier("im"+i, "id", getPackageName());
            ImageView imV = (ImageView) findViewById(id);
            int val = boardValues[i%3][i/3];
            if( val == 1 )
            {
                imV.setVisibility(View.VISIBLE);
                imV.setImageResource(R.drawable.circle);
            }
            else if( val == 2 )
            {
                imV.setVisibility(View.VISIBLE);
                imV.setImageResource(R.drawable.cross);
            }
            else
                imV.setVisibility(View.INVISIBLE);
        }

        if(gameEnabled && (emptyCells > 0))
        {
            textView.setText(getResources().getString(R.string.turn)+": "+((turn == 1)? "O" : "X"));
        }
        else if(gameEnabled && (emptyCells == 0))
        {
            textView.setText(getResources().getString(R.string.draw));
        }
    }

    void saveState()
    {
        /*
        static int[][] boardValues = null;
        static int turn = 0;
        static int computerPlayer = 0;
        static boolean gameEnabled = true;
        static int emptyCells = 9;
         */
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        for (int i = 0; i < 9; i++)
            editor.putInt("p"+i,boardValues[i % 3][i / 3] );
        editor.putInt("turn", turn);
        editor.putInt("computerPlayer", computerPlayer);
        editor.putBoolean("gameEnabled", gameEnabled);
        editor.putInt("emptyCells", emptyCells);

        // Commit the edits!
        editor.apply();
        /*
        try
        {
            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            for (int i = 0; i < 9; i++)
                fos.write(boardValues[i % 3][i / 3]);
            fos.write(turn);
            fos.write(computerPlayer);
            fos.write(gameEnabled ? 1 : 0);
            fos.write(emptyCells);
            fos.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        */
    }

    void readState()
    {
        /*FileInputStream fis = openFileInput(FILENAME);
        for (int i = 0; i < 9; i++)
            boardValues[i % 3][i / 3] = fis.read();
        turn = fis.read();
        computerPlayer = fis.read();
        gameEnabled = (fis.read() == 1);
        emptyCells = fis.read();
        fis.close();
        */
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        for (int i = 0; i < 9; i++)
            boardValues[i % 3][i / 3] = settings.getInt("p"+i, 0);
        turn = settings.getInt("turn", -1);
        computerPlayer = settings.getInt("computerPlayer", -1);
        gameEnabled = settings.getBoolean("gameEnabled", false);
        emptyCells = settings.getInt("emptyCells", -1);
    }
}
