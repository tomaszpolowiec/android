package com.example.kprzystalski.myapplication.libraries

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.httpGet

/**
 * Created by kprzystalski on 13/01/2018.
 */
class Instagram {

    val URL1:String = "https://www.instagram.com/"
    val URL2:String = "/?__a=1"

    fun getImagesList( userName: String,  calbackFun: (ArrayList<String>?) -> Unit ): Unit {
        var imagesList:ArrayList<String>? = ArrayList<String>()
        Fuel.Companion.get(URL1+userName+URL2).responseObject(Insta.InstaDeserializer()) { request, response, result ->
            println("result: "+result)
            val (user, err) = result
            imagesList = user
            calbackFun( imagesList )
        }
    }

}