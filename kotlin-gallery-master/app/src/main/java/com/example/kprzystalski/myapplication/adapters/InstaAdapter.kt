package com.example.kprzystalski.myapplication.adapters

/**
 * Created by kprzystalski on 22/01/2018.
 */

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.example.kprzystalski.myapplication.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.grid_view.view.*

class InstaAdapter(val context: Context, val urlsList :ArrayList<String>?) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        if(urlsList != null) {
            val imageUrl = this.urlsList[position]
            var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var imgView = inflator.inflate(R.layout.grid_view, null)
            //foodView.imgFood.setImageResource(food.image!!)
            Picasso.with(context).load(imageUrl).into(imgView.insta_image)
            return imgView
        }
        return ImageView(context)
    }

    override fun getItem(position: Int): Any {
        if(urlsList != null)
            return urlsList[position]
        else
            return 0
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        if(urlsList != null)
            return urlsList.size
        else
            return 0
    }
}