package com.example.kprzystalski.myapplication.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.kprzystalski.myapplication.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_MESSAGE: String = "userName"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("INFO","Hello")
        button.setOnClickListener {
            Log.d("INFO","Clicked")
            var inp = editText.text
            if(inp.length != 0) {
                val intent = GalleryActivity.newIntent(this)
                intent.putExtra(EXTRA_MESSAGE, inp.toString())
                startActivity(intent)
            }
        }
    }

}
