package com.example.kprzystalski.myapplication.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.GridView
import com.example.kprzystalski.myapplication.R
import com.example.kprzystalski.myapplication.adapters.InstaAdapter
import com.example.kprzystalski.myapplication.libraries.Instagram
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_gallery.*

/**
 * Created by kprzystalski on 07/01/2018.
 */
class GalleryActivity : AppCompatActivity() {

    var imagesArr: ArrayList<String>? = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        val intent: Intent = getIntent()
        var userName = ""
        if(intent != null)
        {
            userName = intent.getStringExtra(MainActivity.EXTRA_MESSAGE)
            println("Username: "+userName)
        }
        Instagram().getImagesList(userName, { res -> getImagesResult(res) })


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    companion object {

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, GalleryActivity::class.java)
            return intent
        }
    }

    fun getImagesResult(arg: ArrayList<String>?)
    {
        imagesArr = arg
        if( arg != null)
            println(arg.size)
        var instaAdapter = InstaAdapter(this, imagesArr)

        imageGrid.adapter = instaAdapter
    }
}