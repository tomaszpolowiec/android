package com.example.tomek93.calculator;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private TextView text1;
    private TextView text2;
    private Button btnCheck;
    private EditText editText;
    MediaPlayer mp = new MediaPlayer();

    int num1= 0;
    int num2= 0;
    String expression= "";
    String exprResult= "";
    private int zakres= 10;
    private int backgroundId = 0;
    Random r = new Random();
    MediaPlayer successPlayer = null;
    MediaPlayer failPlayer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = (TextView) findViewById(R.id.textView);
        text2 = (TextView) findViewById(R.id.textView2);
        btnCheck = (Button) findViewById(R.id.button11);
        editText = (EditText) findViewById(R.id.editText);
        successPlayer = MediaPlayer.create(MainActivity.this, R.raw.succes);
        failPlayer = MediaPlayer.create(MainActivity.this, R.raw.fail);
        newExpresion();
    }

    protected  void numClick(View view)
    {
        if(btnCheck.getText().toString().equals(getResources().getString(R.string.next)) )
            return;
        Button btn = (Button) view;
        String numStr = btn.getText().toString();
        String editVal = editText.getText().toString();
        editText.setText(editVal+numStr);
    }

    int getRandom()
    {
        return r.nextInt(zakres+1);
    }

    void getExpression()
    {
        num1= getRandom();
        num2= getRandom();
        expression= num1+" x "+num2+" =";
        exprResult = (num1*num2)+"";
    }

    void newExpresion()
    {
        getExpression();
        text2.setText("");
        text1.setText(expression);
        editText.setEnabled(true);
        btnCheck.setText(getResources().getString(R.string.check));
    }

    protected  void clickCheck(View view)
    {
        Button btn = (Button) view;
        String btnLabel = btn.getText().toString();
        if( btnLabel.equals(getResources().getString(R.string.check)) )
        {
            checkExpression();
        }
        else if( btnLabel.equals( getResources().getString(R.string.next)) )
        {
            newExpresion();
        }
    }

    void checkExpression()
    {
        String editVal = editText.getText().toString();
        if(editVal.length() == 0)
        {
            text2.setText(getResources().getString(R.string.type));
            return;
        }
        btnCheck.setText(getResources().getString(R.string.next));
        editText.setText("");
        editText.setEnabled(false);
        while(editVal.length() > 1)
        {
           if(editVal.charAt(0) == '0')
           {
                editVal = editVal.substring(1);
           }
           else
               break;
        }

        if( editVal.equals(exprResult))
        {
            text2.setText(num1+" x "+num2+" = "+exprResult+" "+getResources().getString(R.string.good));
            successPlayer.start();
        }
        else
        {
            text2.setText(num1+" x "+num2+" = "+exprResult+" "+getResources().getString(R.string.bad));
            failPlayer.start();
        }
    }

    void optionBtn(View view)
    {
        Intent optionIntent = new Intent(MainActivity.this, OptionActivity.class);
        optionIntent.putExtra("zakres", this.zakres); //Optional parameters
        optionIntent.putExtra("background", this.backgroundId); //Optional parameters
        this.startActivityForResult(optionIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 1
        if(requestCode==1)
        {
            int newZakres = data.getIntExtra("range", 10);
            if( this.zakres != newZakres)
            {
                this.zakres = newZakres;
                newExpresion();
            }

            this.backgroundId = data.getIntExtra("background", 0);
            this.setBackground();
        }

    }

    void setBackground()
    {
        ConstraintLayout layout =(ConstraintLayout) findViewById(R.id.layout1);
        switch (this.backgroundId)
        {
            case 1:
                layout.setBackgroundResource(R.drawable.tlo1);
                break;
            case 2:
                layout.setBackgroundResource(R.drawable.tlo2);
                break;
            case 3:
                layout.setBackgroundResource(R.drawable.tlo3);
                break;
            default:
                layout.setBackground(null);
        }
    }
}
