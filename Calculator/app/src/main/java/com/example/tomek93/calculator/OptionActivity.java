package com.example.tomek93.calculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class OptionActivity extends AppCompatActivity {

    Intent intent= null;
    int zakres= 10;
    int backgroundId= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.zakres = getIntent().getIntExtra("zakres", 10);
        int resID = getResources().getIdentifier("radioButton"+this.zakres, "id", getPackageName());
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
        radioGroup.check(resID);

        this.backgroundId = getIntent().getIntExtra("background", 0);
        //this.setBackground();

        intent = new Intent();
        intent.putExtra("range",this.zakres);
        intent.putExtra("background",this.backgroundId);
        setResult(1,intent);
    }

    void setBackground()
    {
        ConstraintLayout layout =(ConstraintLayout) findViewById(R.id.optionLayout1);
        switch (this.backgroundId)
        {
            case 1:
                layout.setBackgroundResource(R.drawable.tlo1);
                break;
            case 2:
                layout.setBackgroundResource(R.drawable.tlo2);
                break;
            case 3:
                layout.setBackgroundResource(R.drawable.tlo3);
                break;
            default:
                layout.setBackground(null);
        }
    }

    void rangeClick(View view)
    {
        RadioButton radioButton = (RadioButton) view;
        String numStr = radioButton.getText().toString();
        Integer num = Integer.decode( numStr );
        this.zakres = num;

        intent.putExtra("range",this.zakres);
        intent.putExtra("background",this.backgroundId);
        setResult(1,intent);
    }

    void imgClick(View view)
    {
        RadioButton radioButton = (RadioButton) view;
        String numStr = radioButton.getText().toString();
        Integer num = Integer.decode( numStr );
        this.backgroundId= num;
        //this.setBackground();

        intent.putExtra("range",this.zakres);
        intent.putExtra("background",this.backgroundId);
        setResult(1,intent);
        this.finish();
    }
}
